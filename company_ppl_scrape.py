#!/usr/bin/python3
import string
import re
import sys
import html2text
from googlesearch import search
import nltk
from nltk.tokenize import word_tokenize
import requests
import urllib
from companies_house.api import CompaniesHouseAPI
import csv
import pandas as pd
import argparse
from nltk.tokenize import RegexpTokenizer
nltk.download('stopwords')
from nltk.grammar import DependencyGrammar
from nltk.parse import (DependencyGraph, ProjectiveDependencyParser, NonprojectiveDependencyParser)
from nltk import sent_tokenize
from nltk.stem.porter import PorterStemmer
import json
from nameparser.parser import HumanName
from rake_nltk import Rake
        
def companyhouse_check(company_number):
    ch = CompaniesHouseAPI('gc5g__hxIMCSac1Zc50omDixZ-QRB--Q8Wc8xiJx')
    dict_company = ch.get_company(company_number)
    dict_address = ch.get_company_registered_office_address(company_number)
    dict_filing_history = ch.list_company_filing_history(company_number)
    dict_officers = ch.list_company_officers(company_number)
    dict_company_persons = ch.list_company_persons_with_significant_control_statements(company_number)
    dict_establishments = ch.list_company_uk_establishments(company_number)

    with open(company_number + '_companyhouse.json', 'w') as fh:
        json.dump(dict_company, fh)
    with open(company_number + '_companyhouse_addres.json', 'w') as fh:
        json.dump(dict_address, fh)
    with open(company_number + '_companyhouse_filing_history.json', 'w') as fh:
        json.dump(dict_filing_history, fh)
    with open(company_number + '_companyhouse_officers.json', 'w') as fh:
        json.dump(dict_officers, fh)
    with open(company_number + '_companyhouse_persons.json', 'w') as fh:
        json.dump(dict_company_persons, fh)
    with open(company_number + '_companyhouse_establishments.json', 'w') as fh:
        json.dump(dict_establishments, fh)
     
if __name__ == '__main__':
    if len(sys.argv) >= 2:
        companynumber = sys.argv[1]
        companyhouse_check(companynumber)
    else:
        ("python company_house_scrape.py <companynumber>")
