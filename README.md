# companyhouse_scrape

A data scraping tool used for extracting names and important entities from the companyhouse
website. The tool is initially provided an entities and fetches the website. Once fetched,
the web page is extracted, parseed, reformatted, and relies on basic natural language processing
to determine important entities. This information is return as a list of words.

## Prerequisites

`string, re, sys, html2text, googlesearch, nltk, nltk.tokenize, requests, urllib,
companies_house.api, csv, pandas, nltk.download('stopwords'), nltk.grammar, nltk.parse,
nltk.stem.porter, json, nameparser.parser, rake_nltk`

## Example

`python3 company_house_scrape.py <companyhouse number>`

`python3 company_house_scrape.py 10911848`

`python3 company_ppl_scrape.py <companyhouse number>`

`python3 company_ppl_scrape.py 10911848`